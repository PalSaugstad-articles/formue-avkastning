.. raw:: html

   <!-- (php # 2 __DATE__ Formue/avkastning) -->
   <!-- ($attrib_AC=0;) -->

====================
Formue og avkastning
====================

*Av: |author| / |datetime|*

.. include:: revision-header.rst

Civita-økonomen Steinar Juel har gjort endel forsøk på å forklare at formuen i Norge ikke er så skjevt fordelt.
Det siste innlegget er her:
"`Et nytt forsøk på oppklaring
<https://www.civita.no/2019/05/05/et-nytt-forsok-pa-oppklaring>`_".
Her avslutter han med å si at hans måte å se formuesfordelingen på
også kan ha svakheter.
Civita har laget en samlet oversikt over denne saken her:
"`Debatten om formuesfordelingen i Norge
<https://www.civita.no/2019/05/02/temaside-debatten-om-formuesfordelingen-i-norge>`_"

Den største svakheten ved Juels argumentasjon er at han ikke berører *utviklingen* i formuesfordelingen.
Hans måte å regne på viser angivelig at fordelingen ikke er så ille i Norge.
Dermed må vi anta at han bekymrer seg for at formuesfordelingen skal bli skjevere.
Derfor ville det vært naturlig å peke på trender som går i retning av jevnere fordeling.
Det gjør han ikke.
Snarere tvert imot.
Han peker på at
"Det viktigste ved en formue er at den gir avkastning og dermed er en kilde til inntekter"
`her (2019-05-01) <https://www.civita.no/2019/05/01/fellesformuen-utjevner-forskjeller>`_.
Dette er jo den direkte driveren som går i retning av skjevere fordelte formuer:
Så lenge formuer er skjevt fordelt, og det viktigste ved dem er at de gir avkastning i dag, blir de enda skjevere fordelt i morgen.

Statens pensjonsfond, hva er det og hvordan brukes det?
-------------------------------------------------------

`Her (2019-05-02) <https://www.civita.no/2019/05/02/nar-det-pirkes-i-radende-sannheter>`_
bruker han konsekvent benevnelsen **Statens pensjonsfond** om vår felles formue, og skriver:
"Statens pensjonsfond er et fundament for våre pensjoner".
Jeg spurte ham via Twitter, og han svarte at han mener både SPU og SPN når han skriver
"Statens pensjonsfond". SPU er på 9000 milliarder i utenlandske verdipapirer, det mange økonomer
fortsatt kaller Oljefondet. SPN er på 239 Milliarder kroner i nordiske (85% norske) verdipapirer,
og het før "Folketrygdfondet".
Begge skiftet navn i 2006.
Vil det at de skiftet navn si at de faktisk er et fundament for våre pensjoner?
Jeg tror ikke det.
I realiteten fungerer det offentlige pensjons-systemet i Norge etter en modell med løpende inntekter og utgifter over statsbudsjettet.
Dessuten har vi ordninger med privat pensjons-sparing enten individuelt eller gjennom arbeidsgiver.
Ingen midler fra SPU eller SPN er øremerket til bruk for pensjoner.
Riktignok har regjeringen sagt i stortingsmeldinger at Statens pensjonsfond er et fundament for våre pensjoner.
Dette er ikke en realitet idag, og må mer ses på som en mulig framtidig bruk av fondene.
SPU har lenge vært brukt til å balansere statsbudsjettet etter 'handlingsregelen' uten noen særskilt øremerking.
Formålet med forvaltningen av begge fondene er å gi størst mulig avkastning innenfor det til enhver tid gjeldene etiske regelverket.

Formuesfordeling og virkningene av avkastningen
-----------------------------------------------

Hvis all formue i hele verden faktisk var fordelt helt likt på alle verdens husholdninger,
hvor viktig ville det da vært at formuen ga avkastning?
Ved en jevnt fordelt avkastning ville alle komme like bra ut.
Avkastningen ville ikke bety noe i praksis,
for det ville ikke gitt noen av oss fordeler i noe marked,
i og med at det er de varene og tjenestene som faktisk blir produsert som kan konsumeres.
Den ekstra avkastningen er rent finansiell og vil ikke på magisk vis gi noen flere varer og tjenester.

I den reelle verden, er ikke formuen jevnt fordelt.
Avkastning på formue gir de som har formue fordeler i forhold til de som ikke har.
Avkastningen gir større kjøpekraft, som forskyver mulighetene til å skaffe seg varer og tjenester,
i tillegg til medbestemmelse om hvordan midler skal investeres.

Dette betyr (ikke overraskende) at avkastning på formue som er skjevt fordelt
er en brikke i puslespillet som angår økende ulikhet.

Juel viser til at bruken av SPU er utjevnende.
Det han sikter til er at alle nordmenn på noenlunde lik linje drar nytte av avkastningen SPU gir via offentlige budsjetter.
Dette har han rett i.
Men når vi snakker om hele vår nasjons formue,
så er det forholdet mellom nordmenn og folk fra andre nasjoner vi må sammenlikne.
Det er slik at den fordelen vi som nordmenn har, blir en tilsvarende ulempe for alle andre,
nøyaktig på samme måte som med all formue som er fordelt skjevt.
Den reelle fordelen vi her snakker om, er at avkastningen på vår felles formue
gir oss nordmenn økt kjøpekraft av utenlandske varer og tjenester.
Dette er faktisk et tveegget sverd.
Vi har ikke lyst til å bli akterutseilt i forhold til utlandet.
Vi vet at vi må henge med teknologisk.
Ingen kommer til å foreslå at vi kan trekke oss tilbake og nyte fruktene av vår rikdom overfor utlandet.
Nei, vi må henge i, uansett.
Denne betraktningen viser hvor riktig John Maynard Keynes forslag om en supernasjonal valuta (Bancor)
og en 'International Clearing Union' var.
I denne institusjonen ville det blitt lagt inn utjevningsmekanismer mellom nasjoner med ulik grad av eksport,
for å holde nasjonenes finansielle fordringer overfor hverandre under kontroll.

Har det noe å si at formue er skjevt fordelt?
---------------------------------------------

Både Karl Polanyi (1944) og Thomas Piketty (2013) har vist at vi har hatt perioder i verdenshistorien med store ulikheter.
Disse har blitt avløst av perioder med mindre ulikheter.
I overgangene har det en tendens til å være perioder med stor sosial og økonomisk uro,
i mange tilfeller i form av krig og finanskriser.

Økende ulikhet i formuesfordeling (og inntektsfordeling) har skapt enorme problemer før,
og vi ser tendenser til at det skaper problemer nå igjen.

Jeg har ikke sett noen fagøkonomer, hverken Steinar Juel eller andre,
som har skrevet noe som beroliger meg når det gjelder formuesfordeling.
Så lenge formuene gir avkastning blir formuene mer og mer skjevt fordelt.
Hvis formuene *ikke* gir avkastning over en lang periode, ja da har vi havnet i en dyp finanskrise,
og det er som kjent heller ikke noe å trakte etter.

Gordisk knute.

.. raw:: html

   <hr>
   <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Formue og avkastning">Tweet</a>
   <br>
   <a target="_blank" href="show?f=articles/parsed/formue-avkastning/nor/formue-avkastning.pdf">som pdf</a>
